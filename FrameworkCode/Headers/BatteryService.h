/****************************************************************************

  Header file for Battery Display service
  based on the Gen 2 Events and Services Framework

  Michael Tucker 11/25/18

 ****************************************************************************/

#ifndef BatteryService_H
#define BatteryService_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitBattery, BatteryWelcomeMode, Active, BatteryEndOfInteraction } BatteryState_t ;

// Public Function Prototypes
bool InitBatteryService(uint8_t Priority);
bool PostBatteryService(ES_Event_t ThisEvent);
ES_Event_t RunBatteryService(ES_Event_t ThisEvent);

#endif /* BatteryService_H */
