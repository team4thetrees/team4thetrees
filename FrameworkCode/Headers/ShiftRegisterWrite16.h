/****************************************************************************

  Header file for 16 Bit Shift Register service
  based on the Gen 2 Events and Services Framework

  Michael Tucker 11/25/18

 ****************************************************************************/

#ifndef ShiftRegisterWrite_H
#define ShiftRegisterWrite_H

// Public Function Prototypes

// FOR BATTERY
void SR_Init16Bat(void);
void SR_Write16Bat(uint16_t NewValue);

// FOR LEVEL
void SR_Init16Lev(void);
void SR_Write16Lev(uint16_t NewValue);

#endif
