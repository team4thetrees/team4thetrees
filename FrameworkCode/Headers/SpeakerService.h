/****************************************************************************

  Header file for Speaker service
  based on the Gen 2 Events and Services Framework

  Michael Tucker 11/13/18

 ****************************************************************************/

#ifndef SpeakerService_H
#define SpeakerService_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"

// Note frequencies
#define C4 261
#define D4 294
#define E4 330
#define F4 349
#define G4 392
#define A4 440
#define B4 494
#define C5 523
#define D5 587
#define E5 659
#define F5 698
#define G5 784
#define A5 880
#define B5 988
#define C6 1047
#define D6 1175
#define E6 1319
#define F6 1397
#define G6 1568
#define A6 1760
#define B6 1976
#define C7 2093

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitSpeaker, Waiting2Speak, Speaking } SpeakerState_t ;

// Public Function Prototypes

bool InitSpeakerService(uint8_t Priority);
bool PostSpeakerService(ES_Event_t ThisEvent);
ES_Event_t RunSpeakerService(ES_Event_t ThisEvent);
void PlayTune(uint16_t Notes[], uint8_t NumNotes);

#endif /* WindService_H */
