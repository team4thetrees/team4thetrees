/****************************************************************************

  Header file for Tree service
  based on the Gen 2 Events and Services Framework

  Michael Tucker 11/9/18

 ****************************************************************************/

#ifndef TreeService_H
#define TreeService_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitTree, WelcomeMode, TreeActive, EndOfInteraction } TreeState_t ;

// Public Function Prototypes

bool InitTreeService(uint8_t Priority);
bool PostTreeService(ES_Event_t ThisEvent);
ES_Event_t RunTreeService(ES_Event_t ThisEvent);
TreeState_t GetCurrentTreeState(void);

#endif /* TreeService_H */
