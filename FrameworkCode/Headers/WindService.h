/****************************************************************************

  Header file for Wind service
  based on the Gen 2 Events and Services Framework

  Michael Tucker 11/13/18

 ****************************************************************************/

#ifndef WindService_H
#define WindService_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitWind, Waiting2Spin, SpinningByUser, SpinningBySelf } WindState_t ;

// Public Function Prototypes

bool InitWindService(uint8_t Priority);
bool PostWindService(ES_Event_t ThisEvent);
ES_Event_t RunWindService(ES_Event_t ThisEvent);
bool Check4Wind(void);

#endif /* WindService_H */
