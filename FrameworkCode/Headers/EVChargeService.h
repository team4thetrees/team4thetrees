/****************************************************************************

  Header file for EVCharge service
  based on the Gen 2 Events and Services Framework

  Danielle Katz 11/11/18

 ****************************************************************************/

#ifndef EVChargeService_H
#define EVChargeService_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitEVCharge, WaitingForPlug, PluggedCorrect, PluggedWrong } EVChargeState_t ;

// Public Function Prototypes

bool InitEVChargeService(uint8_t Priority);
bool PostEVChargeService(ES_Event_t ThisEvent);
ES_Event_t RunEVChargeService(ES_Event_t ThisEvent);
bool Check4Charge(void);

#endif /* EVChargeService_H */
