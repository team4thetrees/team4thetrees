/****************************************************************************

  Header file for solar service
  based on the Gen 2 Events and Services Framework

	Xiyuan Chen 11/14/18

 ****************************************************************************/

#ifndef SolarService_H
#define SolarService_H

#include "ES_Types.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "TreeService.h"

typedef enum { InitSolar, Debouncing, Ready2Sample } SolarState_t ;

// Public Function Prototypes

bool InitSolarService(uint8_t Priority);
bool PostSolarService(ES_Event_t ThisEvent);
ES_Event_t RunSolarService(ES_Event_t ThisEvent);
bool Check4Sunlight(void);

#endif /* SolarService_H */
