/****************************************************************************

  Header file for LEAF service
  based on the Gen 2 Events and Services Framework

	Danielle Katz 11/09/18

 ****************************************************************************/

#ifndef LEAFService_H
#define LEAFService_H

#include "ES_Types.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "TreeService.h"

typedef enum { InitLEAF, NoLEAFDetected, LEAFDetected } LEAFState_t ;

// Public Function Prototypes

bool InitLEAFService(uint8_t Priority);
bool PostLEAFService(ES_Event_t ThisEvent);
ES_Event_t RunLEAFService(ES_Event_t ThisEvent);
bool Check4LEAF(void);

#endif /* LEAFService_H */
