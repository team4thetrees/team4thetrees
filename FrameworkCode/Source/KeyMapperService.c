/****************************************************************************
 Module
   KeyMapperService.c

 Revision
   1.0.1

 Description
   This is the implementation of the Key Mapper service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/04/18       mpt      Implemented for Lab4
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

// headers for other SMs to post to
#include "KeyMapperService.h"
#include "DecodeMorseService.h"


/*----------------------------- Module Defines ----------------------------*/
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitKeyMapperService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority
 Notes

 Author
     Michael Tucker 11/04/18
****************************************************************************/
bool InitKeyMapperService(uint8_t Priority)
{
  MyPriority = Priority;
  return true;
}

/****************************************************************************
 Function
     PostKeyMapperService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Michael Tucker 11/04/18
****************************************************************************/
bool PostKeyMapperService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunKeyMapperService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT

 Description
  Will act based upon the event passed in map keys to codes to test decode
  morse service

 Notes

 Author
   Michael Tucker 11/04/18
****************************************************************************/
ES_Event_t RunKeyMapperService(ES_Event_t ThisEvent)
{
  static ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  ES_Event_t KeyMapEvent;
  switch (ThisEvent.EventType){
    case ES_NEW_KEY :
      switch (ThisEvent.EventParam){
        case '.' :
          KeyMapEvent.EventType = ES_DOT_DETECTED;
          PostDecodeMorseService(KeyMapEvent);
          break;
        case ',' :
          KeyMapEvent.EventType = ES_DASH_DETECTED;
          PostDecodeMorseService(KeyMapEvent);
          break;
        case '/' :
          KeyMapEvent.EventType = ES_EOC_DETECTED;
          PostDecodeMorseService(KeyMapEvent);
          break;
        case ' ' :
          KeyMapEvent.EventType = ES_EOW_DETECTED;
          PostDecodeMorseService(KeyMapEvent);
          break;
      }
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
