/****************************************************************************
 Module
   SolarService.c

 Revision
   1.0.0

 Description
   This is a file for implementing the solar energy service
	 reporting events to the TreeService.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/14/18 15:47 xc      began conversion from TemplateService.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "SolarService.h"
#include "LEAFService.h"
#include "TreeService.h"
#include "EventCheckers.h"
#include "SpeakerService.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define SOLAR_INPUT_PIN BIT2HI
#define HI true
#define SOLAR_TIMER 10
#define ONE_SEC 976
#define DEBOUNCE_TIMER 9
#define HALF_SEC ONE_SEC/8

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static SolarState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitSolarService

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Michael Tucker 11/25/18
****************************************************************************/
bool InitSolarService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
	CurrentState = InitSolar;

  // Initializes Solar input pin
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;
  while ((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0)
  {
  }
  HWREG(GPIO_PORTA_BASE+GPIO_O_DEN) |= SOLAR_INPUT_PIN;
  HWREG(GPIO_PORTA_BASE+GPIO_O_DIR) &= ~SOLAR_INPUT_PIN;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostSolarService

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostSolarService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunSolarService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Responds when the sunlight approachs the house, detected based on input
	 from the Hall effect sensor circuit.

 Notes

 Author
   Xiyuan Chen, 11/14/18, 15:08
****************************************************************************/
ES_Event_t RunSolarService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  static bool SunDetected = false;

  switch (CurrentState){
    // Initializing solar
    case InitSolar :
      // Solar initialized move to Ready2Sample state
      if ( ThisEvent.EventType == ES_INIT )
      {
        puts("\r\n Sunlight detection initialized \r\n");
        CurrentState = Ready2Sample;
      }
      break;

    // Debouncing input
    case Debouncing :
      // If the Debounce timer times out, moves to ready2sample state
      if ( ThisEvent.EventType == ES_TIMEOUT &&
        ThisEvent.EventParam == DEBOUNCE_TIMER )
      {
        CurrentState = Ready2Sample;
      }

      // If the sun is present and solar timer times out, posts a sunlight
      // detected event and restarts the one second solar timer
      if (SunDetected && ThisEvent.EventType == ES_TIMEOUT &&
        ThisEvent.EventParam == SOLAR_TIMER)
      {
        ES_Event_t SolarEvent;
        SolarEvent.EventType = ES_SUNLIGHT_DETECTED;
        ES_PostList01(SolarEvent);

        ES_Timer_InitTimer(SOLAR_TIMER, ONE_SEC);
      }

      // If reset event occurs, sets SunDetected to false
      if (ThisEvent.EventType == ES_RESET)
      {
        SunDetected = false;
      }
      break;

    // Ready to sample for sunlight detection
    case Ready2Sample :

      // If the sun has been detected and the solar timer timed out
      // post a sunlight detected event to the battery disply and reset timer
      if (SunDetected && ThisEvent.EventType == ES_TIMEOUT &&
        ThisEvent.EventParam == SOLAR_TIMER)
      {
        ES_Event_t SolarEvent;
        SolarEvent.EventType = ES_SUNLIGHT_DETECTED;
        ES_PostList01(SolarEvent);

        ES_Timer_InitTimer(SOLAR_TIMER, ONE_SEC);
      }

      // If the sun has been detected move to debouncing, start debounce timer
      // And pose the sunlight detected event to the battery display
      if( ThisEvent.EventType == ES_SUNLIGHT_DETECTED )
      {
        SunDetected = true;

        ES_Event_t SolarEvent;
        SolarEvent.EventType = ES_SUNLIGHT_DETECTED;
        ES_PostList01(SolarEvent);

        ES_Timer_InitTimer(SOLAR_TIMER, ONE_SEC);

        ES_Timer_InitTimer(DEBOUNCE_TIMER, HALF_SEC);

        CurrentState = Debouncing;
			}

      // If the sun has been removed move to debouncing, start debounce timer
      if (ThisEvent.EventType == ES_SUNLIGHT_REMOVED)
      {
        SunDetected = false;

        ES_Timer_InitTimer(DEBOUNCE_TIMER, HALF_SEC);

        CurrentState = Debouncing;
      }

      // If reset event occurs, sets SunDetected to false
      if (ThisEvent.EventType == ES_RESET)
      {
        SunDetected = false;
      }
      break;
   }

  return ReturnEvent;
}

/****************************************************************************
 Function
    Check4Sunlight

 Parameters
   None

 Returns
   bool : true if detected change, false otherwise

 Description
   checks if sunlight changed states

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
bool Check4Sunlight(void) {
  static bool LastInputState = false; // Previous state of input
	static bool InputState; // State of transresistive circuit
	ES_Event_t ThisEvent;
  bool ReturnVal = false;

  //Read the input ping
  InputState = ((HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA+ALL_BITS)) &
                 SOLAR_INPUT_PIN) == SOLAR_INPUT_PIN);

	if (GetCurrentTreeState() == TreeActive && InputState != LastInputState)
  {
    if (InputState == HI)
    {
      ThisEvent.EventType = ES_SUNLIGHT_DETECTED;
      PostSolarService(ThisEvent);
      ReturnVal = true;
    }
    else
    {
      ThisEvent.EventType = ES_SUNLIGHT_REMOVED;
      PostSolarService(ThisEvent);
      ReturnVal = true;
    }
  }
  LastInputState = InputState;
	return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
