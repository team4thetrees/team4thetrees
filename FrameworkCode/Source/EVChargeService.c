/****************************************************************************
 Module
   EVChargeService.c

 Revision
   1.0.0

 Description
   This is a file for implementing the EVCharge interaction service
	 reporting events to the TreeService.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/11/18 14:55 dk      began conversion from LEAFService.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "EVChargeService.h"
#include "TreeService.h"
#include "LEAFService.h"
#include "WindService.h"
#include "SpeakerService.h"
#include <PWM16Tiva.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define RIGHT_PLUG_INPUT BIT1HI
#define WRONG_PLUG_INPUT BIT2HI
#define HI true

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
bool ReadInputPin(uint8_t Pin);
void PWM_Car_Motor(void);
void Stop_PWM_Car_Motor(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static EVChargeState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitEVChargeService

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Michael Tucker 11/25/18
****************************************************************************/
bool InitEVChargeService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
	CurrentState = InitEVCharge;

  // Initialize input pins for the plugs
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  while ((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1)
  {
  }
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (RIGHT_PLUG_INPUT | WRONG_PLUG_INPUT);
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= ~RIGHT_PLUG_INPUT;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= ~WRONG_PLUG_INPUT;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostEVChargeService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostEVChargeService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunEVChargeService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Responds to insertion and removal of a EVCharge, detected based on input
	 from the trans-resistive circuit.

 Notes

 Author
   Danielle Katz, 11/09/18, 16:48
****************************************************************************/
ES_Event_t RunEVChargeService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState){

    // Initializing the state machine
    case InitEVCharge :
      // If initialized move to WaitingForPlug
      if ( ThisEvent.EventType == ES_INIT )
      {
        puts("\r\n EVCharge Initialized \r\n");
        CurrentState = WaitingForPlug;
      }
      break;

    // Waits for plug to be inserted
    case WaitingForPlug :

      // If the plug is inserted into the wrong outlet while the TREE is active
      // it plays a tone
      if (GetCurrentTreeState() == TreeActive &&
          ThisEvent.EventType == ES_PLUG_INSERTED_WRONG)
      {
        puts("\r\n Wrong answer");

        ES_Event_t ToneEvent;
        ToneEvent.EventType = ES_PLAY_NOTE;
        ToneEvent.EventParam = G5;
        PostSpeakerService(ToneEvent);

        CurrentState = PluggedWrong;
      }

      // If the plug is inserted into the correct outlet while the TREE is
      // active it plays a note and spins the car
      if (GetCurrentTreeState() == TreeActive &&
          ThisEvent.EventType == ES_PLUG_INSERTED_CORRECT)
      {
        puts("\r\n Correct answer");

        PWM_Car_Motor();

        ES_Event_t ToneEvent;
        ToneEvent.EventType = ES_PLAY_NOTE;
        ToneEvent.EventParam = C4;
        PostSpeakerService(ToneEvent);

        CurrentState = PluggedCorrect;
      }
      break;

    // Plug is inserted correctly
		case PluggedCorrect :

      // If reset is called, stops motor
      if (ThisEvent.EventType == ES_RESET)
      {
        Stop_PWM_Car_Motor();
      }

      // If plug is removed from the correct outlet, stops motor and returns to
      // waiting for plug state
			if (ThisEvent.EventType == ES_PLUG_REMOVED &&
          ThisEvent.EventParam == 1)
			{
        puts("\r\n Correct Plug Removed \r\n");

				Stop_PWM_Car_Motor();
        CurrentState = WaitingForPlug;
			}
      break;

    // Plug is inserted incorrectly
    case PluggedWrong :

      // If plug is removed from the wrong outlet returns to
      // waiting for plug state
			if (ThisEvent.EventType == ES_PLUG_REMOVED &&
          ThisEvent.EventParam == 0)
			{
        puts("\r\n Wrong Plug Removed \r\n");

				CurrentState = WaitingForPlug;
			}
      break;
		}

  return ReturnEvent;
}

/****************************************************************************
 Function
   Check4Charge

 Parameters
   None

 Returns
   bool, true if plug is inserted or removed

 Description
   Checks if a plug is inserted or removed into the outlet

 Notes

 Author
   Danielle Katz, 11/09/18, 16:48
****************************************************************************/
bool Check4Charge(void)
{
  // last input from Tiva pin that reads the state of the 'incorrect' outlet
  static bool LastWrongPlugState = false;
  // last input from Tiva pin that reads the state of the 'correct' outlet
  static bool LastRightPlugState = false;
  //input from Tiva pin that reads the state of the 'incorrect' outlet
  static bool WrongPlugState;
  //input from Tiva pin that reads the state of the 'correct' outlet
  static bool RightPlugState;

  // Read in plug states
	WrongPlugState = ReadInputPin(WRONG_PLUG_INPUT);
 	RightPlugState = ReadInputPin(RIGHT_PLUG_INPUT);

  // Return value, default is false
  bool ReturnVal = false;

  ES_Event_t ThisEvent;

  if (WrongPlugState != LastWrongPlugState)
  {
    ReturnVal = true;
    if (WrongPlugState == HI)
    {
      ThisEvent.EventType = ES_PLUG_INSERTED_WRONG;
      PostEVChargeService(ThisEvent);
    }
    else
    {
      ThisEvent.EventType = ES_PLUG_REMOVED;
      ThisEvent.EventParam = 0;
      PostEVChargeService(ThisEvent);
    }
  }
  else if (RightPlugState != LastRightPlugState)
  {
    ReturnVal = true;
    if (RightPlugState == HI)
    {
      ThisEvent.EventType = ES_PLUG_INSERTED_CORRECT;
      PostEVChargeService(ThisEvent);
      ES_PostList01(ThisEvent);
    }
    else
    {
      ThisEvent.EventType = ES_PLUG_REMOVED;
      ThisEvent.EventParam = 1;
      PostEVChargeService(ThisEvent);
    }
  }
  LastRightPlugState = RightPlugState;
  LastWrongPlugState = WrongPlugState;

	return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/
 /****************************************************************************
  Function
     ReadInputPin

  Parameters
    uint8_t Pin: the pin to read the value of

  Returns
    bool, true if HI, false if LO

  Description
    Will read the pin state and return true if HI, false if LO

  Notes

  Author
    Michael Tucker 11/04/18
 ****************************************************************************/
 bool ReadInputPin(uint8_t Pin)
 {
   return (HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS)) & Pin) == Pin;
 }

 /****************************************************************************
  Function
     PWM_Car_Motor

  Parameters
    None

  Returns
    None

  Description
    Turns car motor on

  Notes

  Author
    Michael Tucker 11/25/18
 ****************************************************************************/
 void PWM_Car_Motor(void)
{
  PWM_TIVA_SetFreq(500, 0);
  PWM_TIVA_SetDuty(100, 0);
}

/****************************************************************************
 Function
    Stop_PWM_Car_Motor

 Parameters
   None

 Returns
   None

 Description
   Turns car motor off

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void Stop_PWM_Car_Motor(void)
{
  PWM_TIVA_SetDuty(0, 0);
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
