/****************************************************************************
 Module
   TreeService.c

 Revision
   1.0.1

 Description
   This is the file to implement the Tree service.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/09/18       mpt      adapted for Tree project
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "TreeService.h"
#include "LEAFService.h"
#include <PWM16Tiva.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/*----------------------------- Module Defines ----------------------------*/
#define TIMER_CCW_POS 3050
#define TIMER_CW_POS 600
#define ONE_SEC 976
#define ONE_MIN 60*ONE_SEC
#define TREE_TIMER 15

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void StartTimer(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static TreeState_t CurrentState = InitTree;

// Bool to keep track of hourglass position
static bool Timer_Is_CCW;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTreeService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves the priority, initializes some variables

 Notes

 Author
     Michael Tucker 11/09/18
****************************************************************************/
bool InitTreeService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;

  CurrentState = InitTree;

  Timer_Is_CCW = true;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }

}

/****************************************************************************
 Function
     PostTreeService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Michael Tucker 11/09/18
****************************************************************************/
bool PostTreeService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTreeService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT

 Description
   Will act based upon the event passed in and the current state to run the
   Tree SM

 Author
   Michael Tucker 11/09/18
****************************************************************************/
ES_Event_t RunTreeService(ES_Event_t ThisEvent)
{
  static ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  static TreeState_t NextState;

  NextState = CurrentState;

  switch (CurrentState){
    // Initializing the TREE
    case InitTree :
      // If initialized, moves to WelcomeMode
      if ( ThisEvent.EventType == ES_INIT )
      {
        puts("\r\n TREE Initialized");
        NextState = WelcomeMode;
      }
      break;

    // Welcome mode waits for the tree to be active
    case WelcomeMode :
      // If the leaf is detected, moves to the active state, starts tree timer
      if (ThisEvent.EventType == ES_LEAF_DETECTED)
      {
        puts("\r\n TREE ACTIVE");
        StartTimer();
        NextState = TreeActive;
      }
      break;

    // Tree is active and interacting with user
    case TreeActive :
      // If the tree timer times out, posts end Tree time done event and moves
      // to end of interaction state
      if ( ThisEvent.EventType == ES_TIMEOUT)
      {
        puts("\r\n TREE TIME DONE");

        ES_Event_t EndOfInteractionEvent;
        EndOfInteractionEvent.EventType = ES_TREE_TIME_DONE;
        EndOfInteractionEvent.EventParam = 0;
        ES_PostList03(EndOfInteractionEvent);

        NextState = EndOfInteraction;
      }

      // If all interactions are completed, moves to end of interaction
      if ( ThisEvent.EventType == ES_ALL_ACTIONS_COMPLETE )
      {
        NextState = EndOfInteraction;
			}

      // If the leaf is removed, post reset event, stop timer, move to
      // welcome mode
      if ( ThisEvent.EventType == ES_LEAF_REMOVED )
      {
        puts("\r\n RESET");

        ES_Event_t ResetEvent;
        ResetEvent.EventType = ES_RESET;
        ResetEvent.EventParam = 0;
        ES_PostList00(ResetEvent);

        ES_Timer_StopTimer(TREE_TIMER);

        NextState = WelcomeMode;
			}

      // If a reset event is received, go to WelcomeMode and stop tree timer
      if (ThisEvent.EventType == ES_RESET )
      {
        puts("\r\n RESET");
        NextState = WelcomeMode;
        ES_Timer_StopTimer(TREE_TIMER);
      }
      break;

    // End of interaction state
    case EndOfInteraction :
      // If the leaf is removed, post reset event, move to welcome mode
      if ( ThisEvent.EventType == ES_LEAF_REMOVED )
      {
        puts("\r\n RESET");

        ES_Event_t ResetEvent;
        ResetEvent.EventType = ES_RESET;
        ResetEvent.EventParam = 0;
        ES_PostList00(ResetEvent);

        NextState = WelcomeMode;
			}
      break;
  }

  CurrentState = NextState;

  return ReturnEvent;
}

/****************************************************************************
 Function
    GetCurrentTreeState

 Parameters
   None

 Returns
   TreeState_t, CurrentState

 Description
   Will return the CurrentState

 Author
   Michael Tucker 11/13/18
****************************************************************************/
TreeState_t GetCurrentTreeState(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

 /****************************************************************************
  Function
     StartTimer

  Parameters
    None

  Returns
    None

  Description
    Turns over hourglass and starts 60s timer
    
  Notes

  Author
    Michael Tucker 11/25/18
 ****************************************************************************/
void StartTimer(void)
{
  PWM_TIVA_SetPeriod(25000, 1);
  if (Timer_Is_CCW)
  {
    PWM_TIVA_SetPulseWidth(TIMER_CW_POS, 3);
    Timer_Is_CCW = false;
  }
  else
  {
    PWM_TIVA_SetPulseWidth(TIMER_CCW_POS, 3);
    Timer_Is_CCW = true;
  }
  ES_Timer_InitTimer(TREE_TIMER, ONE_MIN);
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
