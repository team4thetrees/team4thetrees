/****************************************************************************
 Module
   ShiftRegisterWrite16.c

 Revision
   1.0

 Description
   Writes to a 16 bit shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/15/2018      kevin   built and tested successfully (2:40 pm)
****************************************************************************/

//for test only
#include "termio.h"

#include "EnablePA25_PB23_PD7_PF0.h"

// the common headers for C99 types

#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

// My own header
#include "ShiftRegisterWrite16.h"

// readability defines: OLD, MAY DELETE
// adjust based on what pin of the port you use
#define DATA GPIO_PIN_0
#define DATA_HI BIT0HI
#define DATA_LO BIT0LO

#define SCLK GPIO_PIN_1
#define SCLK_HI BIT1HI
#define SCLK_LO BIT1LO

#define RCLK GPIO_PIN_2
#define RCLK_LO BIT2LO
#define RCLK_HI BIT2HI

// READABILITY DEFINES
//BATTERY: ALL ON F
#define DATA_BAT GPIO_PIN_4
#define DATA_BAT_HI BIT4HI
#define DATA_BAT_LO BIT4LO

#define SCLK_BAT GPIO_PIN_2
#define SCLK_BAT_HI BIT2HI
#define SCLK_BAT_LO BIT2LO

#define RCLK_BAT GPIO_PIN_3
#define RCLK_BAT_LO BIT3LO
#define RCLK_BAT_HI BIT3HI
//LEVEL: DATA ON D, REST ON F
#define DATA_LEV GPIO_PIN_7
#define DATA_LEV_HI BIT7HI
#define DATA_LEV_LO BIT7LO

#define SCLK_LEV GPIO_PIN_1
#define SCLK_LEV_HI BIT1HI
#define SCLK_LEV_LO BIT1LO

#define RCLK_LEV GPIO_PIN_0
#define RCLK_LEV_LO BIT0LO
#define RCLK_LEV_HI BIT0HI


#define GET_MSB_IN_LSB(x) ((x & 0x80)>>7)
//NEWCODE: messing with this function
#define GET_MSB_IN_LSB16(x) ((x & 0x8000)>>15)

// Define ALL_BITS in order to access individual pins on the port.
#define ALL_BITS (0xff<<2)

// PART A: FOR BATTERY DISPLAY
// THREE FUNCTIONS PRESENT HERE
// Initialization step
void SR_Init16Bat(void){

  // set up port F by enabling the peripheral clock
  HWREG(SYSCTL_RCGCGPIO)|= SYSCTL_RCGCGPIO_R5;

  // Waiting for the peripheral to be ready
  while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5)
  {}

  // setting the direction to output for PF4, PF3, PF2
  HWREG(GPIO_PORTF_BASE+GPIO_O_DEN) |= (DATA_BAT_HI|SCLK_BAT_HI|RCLK_BAT_HI);
  HWREG(GPIO_PORTF_BASE+GPIO_O_DIR) |= (DATA_BAT_HI|SCLK_BAT_HI|RCLK_BAT_HI);


  // start with the data & sclk lines low
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (DATA_BAT_LO&SCLK_BAT_LO);

  // start with rclk line high
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= (RCLK_BAT_HI);
}

// NEWCODE
void SR_Write16Bat(uint16_t NewValue){

  uint8_t BitCounter;

  //I am using reading_output to manipulate NewValue temporarily while reading it.
  uint16_t reading_output;
  reading_output = NewValue;

// lower the register clock
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (RCLK_BAT_LO);

// shift out the data while pulsing the serial clock
  BitCounter = 0;
  while(BitCounter < 16) //Do this 16 times
  {
    //GET_MSB_IN_LSB(NewValue) shifts leftmost bit to right.
    //The first 15 bits become 0.
    //If that expression != 0, then right bit must be high. We set data high.
    //Otherwise, data is set low.
    if(GET_MSB_IN_LSB16(reading_output) != 0)
		{
			HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= (DATA_BAT_HI);
		}
    else
		{
			HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (DATA_BAT_LO);
		}

	// raise and lower SCLK
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= (SCLK_BAT_HI);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (SCLK_BAT_LO);

    BitCounter = BitCounter+1;

    //In order to access new data in the next loop, we shift reading_output << 1.
    reading_output = reading_output<<1;
  }

// raise the register clock to latch the new data
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= (RCLK_BAT_HI);
}





// PART B: FOR LEVEL DISPLAY
// THREE FUNCTIONS PRESENT HERE
// Initialization step
void SR_Init16Lev(void){

  PortFunctionInit();

  // set up port F and D by enabling the peripheral clock
  HWREG(SYSCTL_RCGCGPIO)|= SYSCTL_RCGCGPIO_R5;
  HWREG(SYSCTL_RCGCGPIO)|= SYSCTL_RCGCGPIO_R3;

  // Waiting for the peripheral to be ready
  while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5)
  {}

  // setting the direction to output for PD7, PF1, PF0
  HWREG(GPIO_PORTF_BASE+GPIO_O_DEN) |= (SCLK_LEV_HI|RCLK_LEV_HI);
  HWREG(GPIO_PORTF_BASE+GPIO_O_DIR) |= (SCLK_LEV_HI|RCLK_LEV_HI);

  HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= DATA_LEV_HI;
  HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) |= DATA_LEV_HI;

  // start with the data & sclk lines low
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (SCLK_LEV_LO);
  HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA + ALL_BITS)) &= (DATA_LEV_LO);

  // start with rclk line high
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= (RCLK_LEV_HI);
}

// NEWCODE
void SR_Write16Lev(uint16_t NewValue){

  uint8_t BitCounter;

  //I am using reading_output to manipulate NewValue temporarily while reading it.
  uint16_t reading_output;
  reading_output = NewValue;

// lower the register clock
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (RCLK_LEV_LO);

// shift out the data while pulsing the serial clock
  BitCounter = 0;
  while(BitCounter < 16) //Do this 16 times
  {
    //GET_MSB_IN_LSB(NewValue) shifts leftmost bit to right.
    //The first 15 bits become 0.
    //If that expression != 0, then right bit must be high. We set data high.
    //Otherwise, data is set low.
    if(GET_MSB_IN_LSB16(reading_output) != 0)
		{
			HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA + ALL_BITS)) |= (DATA_LEV_HI);
		}
    else
		{
			HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA + ALL_BITS)) &= (DATA_LEV_LO);
		}

	// raise and lower SCLK
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= (SCLK_LEV_HI);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (SCLK_LEV_LO);

    BitCounter = BitCounter+1;

    //In order to access new data in the next loop, we shift reading_output << 1.
    reading_output = reading_output<<1;
  }

// raise the register clock to latch the new data
  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= (RCLK_LEV_HI);
}

#endif
