/****************************************************************************
 Module
   BatteryService.c

 Revision
   1.0.1

 Description
   This is the file to implement the Battery service.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/25/18       mpt      adapted for Tree project
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "BatteryService.h"
#include "ShiftRegisterWrite16.h"
#include "SpeakerService.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "ES_DeferRecall.h"

/*----------------------------- Module Defines ----------------------------*/
#define NUM_DISPLAY_STATES 6
#define BATTERY_TIMER 11
#define INACTION_TIMER 8
#define ONE_SEC 976
#define QUARTER_SEC ONE_SEC/4
#define THIRTY_SEC 30*ONE_SEC

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void ResetDisplay(void);
static void DisplayColumns(uint8_t GreenLevel,
                           uint8_t YellowLevel,
                           uint8_t RedLevel);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

// Current state to track where we are in SM
static BatteryState_t CurrentState = InitBattery;

// Values to write to the 16 bit shift register to display the different
// battery states
static const uint16_t BatteryStates[] = {0x0001, 0x0021, 0x008A,
                                         0x808A, 0x3144, 0x3944 };

// Preset looks for the columns of LEDs for the welcome mode {G, Y, R}
static const uint16_t ColumnStates[][3] = {{0,0,5},{1,1,4},{2,2,3},
                                           {3,3,2},{4,4,1},{5,5,0}};

// Values to write to the 16 bit shift register to display 0-5 LEDs for each
// column
static const uint16_t WIND[] = {0x0000, 0x0010, 0x0030,
                                0x0070, 0x00F0, 0x00F1};
static const uint16_t SOLAR[] = {0x0000, 0x4000, 0x4008,
                                 0x4108, 0x410A, 0x410E};
static const uint16_t COAL[] = {0x0000, 0x2000, 0x2200,
                                0x2600, 0x2E00, 0x3E00};

// Variables to track progress
static uint8_t DisplayState = 0;
static uint8_t NumActivitiesDone = 0;
static uint8_t WindLevel = 0;
static uint8_t SolarLevel = 0;
static uint8_t CoalLevel = 5;
static bool WindDetected = false;
static bool PlugDetected = false;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitBatteryService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves the priority, sets up shift registers and initializes variables

 Notes

 Author
     Michael Tucker 11/25/18
****************************************************************************/
bool InitBatteryService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  // Initialize variables
  MyPriority = Priority;
  NumActivitiesDone = 0;
  DisplayState = 0;

  WindLevel = 0;
  SolarLevel = 0;
  CoalLevel = 5;

  WindDetected = false;
  WindDetected = false;
  PlugDetected = false;

  // Initialize shift registers
  SR_Init16Bat();
  SR_Write16Bat(BatteryStates[DisplayState]);
  SR_Init16Lev();
  DisplayColumns(ColumnStates[DisplayState][0],
                 ColumnStates[DisplayState][1],
                 ColumnStates[DisplayState][2]);

  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
    return true;
  }else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostBatteryService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Michael Tucker 11/25/18
****************************************************************************/
bool PostBatteryService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunBatteryService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT

 Description
   Will act based upon the event passed in and the current state to run the
   Battery SM

 Author
   Michael Tucker 11/25/18
****************************************************************************/
ES_Event_t RunBatteryService(ES_Event_t ThisEvent)
{
  static ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  static BatteryState_t NextState;
  static bool BatteryOn = true;

  NextState = CurrentState;

  switch (CurrentState){

    // If we initialize the battery display, we move to welcome mode
    case InitBattery :
      if ( ThisEvent.EventType == ES_INIT )
      {
        puts("\r\n INIT_BATTERY");
        NextState = BatteryWelcomeMode;
        ES_Timer_InitTimer(BATTERY_TIMER, QUARTER_SEC);
      }
      break;

    // Welcome mode displays LED chases to allure ARBORISTS
    case BatteryWelcomeMode :
      // If the leaf is detected, clears the displays, sets next state to Active
      // And starts the inaction timer
      if (ThisEvent.EventType == ES_LEAF_DETECTED)
      {
        SR_Write16Bat(BatteryStates[0]);
        DisplayColumns(WindLevel, SolarLevel, CoalLevel);
        ES_Timer_InitTimer(INACTION_TIMER, THIRTY_SEC);
        NextState = Active;
      }

      // If the Battery timer runs out, it will change the displays to the next
      // and reset the battery timer
      if (ThisEvent.EventType == ES_TIMEOUT &&
          ThisEvent.EventParam == BATTERY_TIMER)
      {
        DisplayState = (DisplayState+1)%NUM_DISPLAY_STATES;
        SR_Write16Bat(BatteryStates[DisplayState]);
        DisplayColumns(ColumnStates[DisplayState][0],
                       ColumnStates[DisplayState][1],
                       ColumnStates[DisplayState][2]);
        ES_Timer_InitTimer(BATTERY_TIMER, QUARTER_SEC);
      }

      // If a reset is called, it will reset the displays
      if (ThisEvent.EventType == ES_RESET)
      {
        ResetDisplay();
      }
      break;

    // Active mode runs the displays during the interactions
    case Active :
      // If the user hasn't interacted with the TREE for 30 seconds, it resets
      if (ThisEvent.EventType == ES_TIMEOUT &&
          ThisEvent.EventParam == INACTION_TIMER)
      {
        ResetDisplay();
        uint16_t tune[6] = {0, C5, 0, C5, 0, C5};
        PlayTune(tune,6);
        ES_Event_t Inaction;
        Inaction.EventType = ES_RESET;
        Inaction.EventParam = 0;
        ES_PostList00(Inaction);
        NextState = BatteryWelcomeMode;
      }

      // If Wind is detected update the display accordingly
      if ( ThisEvent.EventType == ES_WIND_DETECTED )
      {
        if (!WindDetected)
        {
          WindDetected = true;
          NumActivitiesDone++;
          SR_Write16Bat(BatteryStates[NumActivitiesDone]);
        }

        WindLevel = ThisEvent.EventParam;

        if (CoalLevel-((WindLevel+1)/2) < 0)
        {
          DisplayColumns(WindLevel, SolarLevel, 0);
        }
        else
        {
          DisplayColumns(WindLevel, SolarLevel, CoalLevel-((WindLevel)/2));
        }

        ES_Timer_InitTimer(INACTION_TIMER, THIRTY_SEC);
      }

      // If the wind turbine starts spinning by itself, update display
      if ( ThisEvent.EventType == ES_SPINNING )
      {
        if (CoalLevel-((WindLevel+1)/2) < 0)
        {
          CoalLevel = 0;
        }
        else
        {
          CoalLevel -= ((WindLevel+1)/2);
        }

        DisplayColumns(WindLevel, SolarLevel, CoalLevel);

        NumActivitiesDone++;
        SR_Write16Bat(BatteryStates[NumActivitiesDone]);

        // If this is the last interaction, post all actions complete
        if (NumActivitiesDone == (NUM_DISPLAY_STATES - 1))
        {
          ES_Event_t ActionsComplete;
          ActionsComplete.EventType = ES_ALL_ACTIONS_COMPLETE;
          ActionsComplete.EventParam = 0;
          ES_PostList02(ActionsComplete);
        }
      }

      // If the correct plug is detected, update display accordingly
      if ( ThisEvent.EventType == ES_PLUG_INSERTED_CORRECT )
      {
        if (!PlugDetected)
        {
          PlugDetected = true;

          NumActivitiesDone++;
          SR_Write16Bat(BatteryStates[NumActivitiesDone]);

          // If this is the last interaction, post all actions complete
          if (NumActivitiesDone == (NUM_DISPLAY_STATES - 1))
          {
            ES_Event_t ActionsComplete;
            ActionsComplete.EventType = ES_ALL_ACTIONS_COMPLETE;
            ActionsComplete.EventParam = 0;
            ES_PostList02(ActionsComplete);
          }
        }

        ES_Timer_InitTimer(INACTION_TIMER, THIRTY_SEC);
      }

      // If the sunlight is detected, update display accordingly
      if ( ThisEvent.EventType == ES_SUNLIGHT_DETECTED )
      {
        if (SolarLevel < 5)
        {
          ES_Event_t ToneEvent;
          ToneEvent.EventType = ES_PLAY_NOTE;
          ToneEvent.EventParam = C4;
          PostSpeakerService(ToneEvent);

          SolarLevel++;

          if ((SolarLevel % 2) == 1 && CoalLevel > 0)
          {
            CoalLevel--;
          }

          DisplayColumns(WindLevel, SolarLevel, CoalLevel);

          if (SolarLevel == 1)
          {
            NumActivitiesDone++;
            SR_Write16Bat(BatteryStates[NumActivitiesDone]);
          }

          if (SolarLevel == 5)
          {
            NumActivitiesDone++;
            SR_Write16Bat(BatteryStates[NumActivitiesDone]);

            // If this is the last interaction, post all actions complete
            if (NumActivitiesDone == (NUM_DISPLAY_STATES - 1))
            {
              ES_Event_t ActionsComplete;
              ActionsComplete.EventType = ES_ALL_ACTIONS_COMPLETE;
              ActionsComplete.EventParam = 0;
              ES_PostList02(ActionsComplete);
            }
          }

          ES_Timer_InitTimer(INACTION_TIMER, THIRTY_SEC);
        }
      }

      // If all actions are done or 60s timer is done, go to end of interaction
      if ( ThisEvent.EventType == ES_ALL_ACTIONS_COMPLETE ||
          ThisEvent.EventType == ES_TREE_TIME_DONE)
      {
        uint16_t tune[7] = {B5, A5, G5, F5, G5, A5, B5};
        PlayTune(tune,7);

        ES_Timer_InitTimer(BATTERY_TIMER, QUARTER_SEC);

        NextState = BatteryEndOfInteraction;
      }

      // If a reset is called, reset displays and go to welcome mode
      if (ThisEvent.EventType == ES_RESET)
      {
        ResetDisplay();
        NextState = BatteryWelcomeMode;
      }
      break;

    // BatteryEndOfInteraction will update the display at end of interactions
    case BatteryEndOfInteraction :
      // If a reset is called, reset displays and go to welcome mode
      if (ThisEvent.EventType == ES_RESET)
      {
        ResetDisplay();
        NextState = BatteryWelcomeMode;
      }

      // If the user hasn't interacted with the TREE for 30 seconds, it resets
      if (ThisEvent.EventType == ES_TIMEOUT &&
          ThisEvent.EventParam == INACTION_TIMER)
      {
        ResetDisplay();

        ES_Event_t Inaction;
        Inaction.EventType = ES_RESET;
        Inaction.EventParam = 0;
        ES_PostList00(Inaction);

        uint16_t tune[6] = {0, C5, 0, C5, 0, C5};
        PlayTune(tune,6);

        NextState = BatteryWelcomeMode;
      }

      // If the Battery timer runs out, it will turn on/off the LEDs
      if (ThisEvent.EventType == ES_TIMEOUT &&
          ThisEvent.EventParam == BATTERY_TIMER)
      {
        if (BatteryOn) {
          SR_Write16Bat(0x0000);
          DisplayColumns(0, 0, 0);
          BatteryOn = false;
        }
        else
        {
          SR_Write16Bat(BatteryStates[NumActivitiesDone]);
          DisplayColumns(WindLevel, SolarLevel, CoalLevel);
          BatteryOn = true;
        }
        ES_Timer_InitTimer(BATTERY_TIMER, 2*QUARTER_SEC);
      }
      break;
  }

  CurrentState = NextState;

  return ReturnEvent;
}


/***************************************************************************
 private functions
 ***************************************************************************/

 /****************************************************************************
  Function
     ResetDisplay

  Parameters
    None

  Returns
    None

  Description
    Resets variables, updates displays, starts timer

  Author
    Michael Tucker 11/25/18
 ****************************************************************************/
void ResetDisplay(void)
{
  // Reset variables
  NumActivitiesDone = 0;
  DisplayState = 0;
  WindLevel = 0;
  SolarLevel = 0;
  CoalLevel = 5;
  WindDetected = false;
  PlugDetected = false;

  // Update displays
  DisplayColumns(WindLevel, SolarLevel, CoalLevel);
  SR_Write16Bat(BatteryStates[DisplayState]);

  // Starts timer
  ES_Timer_InitTimer(BATTERY_TIMER, QUARTER_SEC);
}

/****************************************************************************
 Function
    DisplayColumns

 Parameters
   uint8_t GreenLevel : How many green (wind) LEDs to be lit
   uint8_t YellowLevel : How many yellow (solar) LEDs to be lit
   uint8_t RedLevel : How many red (coal) LEDs to be lit

 Returns
   None

 Description
   Resets variables, updates displays, starts timer

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void DisplayColumns(uint8_t GreenLevel, uint8_t YellowLevel, uint8_t RedLevel)
{
  SR_Write16Lev(WIND[GreenLevel] | SOLAR[YellowLevel] | COAL[RedLevel]);
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
