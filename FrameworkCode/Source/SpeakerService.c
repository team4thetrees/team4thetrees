/****************************************************************************
 Module
   SpeakerService.c

 Revision
   1.0.1

 Description
   This is the file to implement the Speaker service.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/13/18       mpt      adapted for Tree project
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "SpeakerService.h"
#include <PWM16Tiva.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "ES_DeferRecall.h"

// headers for other services to post to
#include "TreeService.h"

/*----------------------------- Module Defines ----------------------------*/
#define SPEAKER_TIMER 13
#define ONE_SEC 976
#define QUARTER_SEC ONE_SEC/4

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void PlayTone(uint16_t freq);
static void StopTone(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static SpeakerState_t CurrentState = InitSpeaker;
// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event_t DeferralQueue[8+1];

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitSpeakerService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves the priority, sets up Speaker pins

 Notes

 Author
     Michael Tucker 11/13/18
****************************************************************************/
bool InitSpeakerService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;

  // initialize the deferal queue
	ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));

  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
    return true;
  }else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostSpeakerService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Michael Tucker 11/13/18
****************************************************************************/
bool PostSpeakerService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunSpeakerService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT

 Description
   Will act based upon the event passed in and the current state to run the
   Speaker SM

 Author
   Michael Tucker 11/13/18
****************************************************************************/
ES_Event_t RunSpeakerService(ES_Event_t ThisEvent)
{
  static ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  static SpeakerState_t NextState;

  NextState = CurrentState;

  switch (CurrentState){
    // Initializing the speaker SM
    case InitSpeaker :
      // If speaker is initialized, moves to waiting2Speak
      if ( ThisEvent.EventType == ES_INIT )
      {
        puts("\r\n INIT_SPEAKER");
        NextState = Waiting2Speak;
      }
      break;

    // Waiting to play note
    case Waiting2Speak :
      // If play note event, plays the note, starts 1/4s speaker timer
      // moves to speaking state
      if (ThisEvent.EventType == ES_PLAY_NOTE)
      {
        PlayTone(ThisEvent.EventParam);
        ES_Timer_InitTimer(SPEAKER_TIMER, QUARTER_SEC);
        NextState = Speaking;
      }
      break;

    // Playing a note
    case Speaking :
      // If a play note event occurs, enqueue it to the deferral queue
      if (ThisEvent.EventType == ES_PLAY_NOTE)
      {
        ES_EnQueueLIFO(DeferralQueue, ThisEvent);
      }

      // If the speaker timer times out, or a reset occurs, stops tone
      // and moves to waiting2speak state
      if ( ThisEvent.EventType == ES_TIMEOUT ||
        ThisEvent.EventType == ES_RESET )
      {
        StopTone();
        ES_RecallEvents(MyPriority, DeferralQueue);
        NextState = Waiting2Speak;
      }
      break;
  }

  CurrentState = NextState;

  return ReturnEvent;
}

/****************************************************************************
 Function
   PlayTune

 Parameters
   uint16_t Notes[] : array of notes to play in order
   uint8_t NumNotes : number of notes to play

 Returns
   None

 Description
   Plays the array of notes by posting them as Play Note events

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void PlayTune(uint16_t Notes[], uint8_t NumNotes)
{
  static uint8_t i = 0;
  for (i = 0; i < NumNotes; i++)
  {
    ES_Event_t ToneEvent;
    ToneEvent.EventType   = ES_PLAY_NOTE;
    ToneEvent.EventParam  = Notes[i];
    PostSpeakerService(ToneEvent);
  }
  ES_Timer_InitTimer(SPEAKER_TIMER, QUARTER_SEC);
}


/***************************************************************************
 private functions
 ***************************************************************************/

 /****************************************************************************
  Function
    PlayTone

  Parameters
    uint16_t freq : frequency of the note to play

  Returns
    None

  Description
    PWMs the speaker at the specified frequency

  Notes

  Author
    Michael Tucker 11/25/18
 ****************************************************************************/
void PlayTone(uint16_t freq)
{
  PWM_TIVA_SetFreq(freq, 4);
  PWM_TIVA_SetDuty(50, 8);
}

/****************************************************************************
 Function
   StopTone

 Parameters
   None

 Returns
   None

 Description
   Stops PWM of speaker

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void StopTone(void)
{
  PWM_TIVA_SetFreq(500, 4);
  PWM_TIVA_SetDuty(0, 8);
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
