/****************************************************************************
 Module
   WindService.c

 Revision
   1.0.1

 Description
   This is the file to implement the Wind service.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/13/18       mpt      adapted for Tree project
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "WindService.h"
#include <ADMulti.h>
#include <PWM16Tiva.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

// headers for other services to post to
#include "TreeService.h"
#include "SpeakerService.h"

/*----------------------------- Module Defines ----------------------------*/
#define PIN_BASE GPIO_PORTE_BASE
#define RELAY_PIN BIT2HI
#define WIND_TIMER 14
#define ONE_SEC 976
#define TEN_SEC 10*ONE_SEC


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void SpinMotor(void);
static void StopMotor(void);
static void PWM_Motor(void);
static void Stop_PWM_Motor(void);
static void RelayON(void);
static void RelayOFF(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static WindState_t CurrentState = InitWind;
static uint8_t LastWindSpeed = 0;


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitWindService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves the priority, sets up the morse signal pin as an input.

 Notes

 Author
     Michael Tucker 11/13/18
****************************************************************************/
bool InitWindService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;

  // Initializes the output for the relay
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R4;
  while ((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R4) != SYSCTL_PRGPIO_R4)
  {
  }
  HWREG(PIN_BASE+GPIO_O_DEN) |= RELAY_PIN;
  HWREG(PIN_BASE+GPIO_O_DIR) |= RELAY_PIN;

  // Turns the relay off
  RelayOFF();

  // Sets up the 1 analog input
  ADC_MultiInit(1);

  // Initializes 9 PWM channels and sets the frequency and duty cycle for the
  // wind turbine motor to be open
  PWM_TIVA_Init(9);
  PWM_TIVA_SetFreq(500, 2);
  PWM_TIVA_SetDuty(100, 4);

  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
    return true;
  }else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostWindService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Michael Tucker 11/13/18
****************************************************************************/
bool PostWindService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunWindService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT

 Description
   Will act based upon the event passed in and the current state to run the
   Wind SM

 Author
   Michael Tucker 11/13/18
****************************************************************************/
ES_Event_t RunWindService(ES_Event_t ThisEvent)
{
  static ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  static WindState_t NextState;

  NextState = CurrentState;

  switch (CurrentState){

    // Initializing wind turbine
    case InitWind :
      // If initialized, moves to Waiting2Spin state
      if ( ThisEvent.EventType == ES_INIT )
      {
        puts("\r\n WIND_INIT");
        NextState = Waiting2Spin;
      }
      break;

    // Waiting for the turbine to be spun
    case Waiting2Spin :

      // If wind is detected while the TREE is active, moves to SpinningByUser
      // state and starts the ten second wind timer
      if (ThisEvent.EventType == ES_WIND_DETECTED &&
        GetCurrentTreeState() == TreeActive)
      {
        puts("\r\n WIND");
        NextState = SpinningByUser;
        ES_Timer_InitTimer(WIND_TIMER, TEN_SEC);
      }
      break;

    // Turbine is spinning by user
    case SpinningByUser :

      // If the wind timer times out, spins motor by self, posts that event to
      // list 01, plays a tune, and moves to spinning by self state
      if ( ThisEvent.EventType == ES_TIMEOUT &&
        ThisEvent.EventParam == WIND_TIMER )
      {
        puts("\r\n SPIN");

        SpinMotor();

        uint16_t tune[4] = {C4, C5, C6, C7};
        PlayTune(tune,4);

        ES_Event_t ThisEvent;
        ThisEvent.EventType   = ES_SPINNING;
        ES_PostList01(ThisEvent);

        NextState = SpinningBySelf;
			}

      // If reset is called, stops motor and moves to Waiting2Spin State
      if ( ThisEvent.EventType == ES_RESET )
      {
        StopMotor();
        NextState = Waiting2Spin;
			}
      break;

    // Wind turbine is spinning by itself
    case SpinningBySelf :
      // If reset is called, stops motor and moves to Waiting2Spin State
      if ( ThisEvent.EventType == ES_RESET )
      {
        StopMotor();
        NextState = Waiting2Spin;
			}
      break;
  }

  CurrentState = NextState;

  return ReturnEvent;
}

/****************************************************************************
 Function
   Check4Wind

 Parameters
   None

 Returns
   bool, true if wind is detected, false otherwise

 Description
   Checks to see if wind is detected, and if so, how much wind, it will play
   a different note based on how much wind is detected

 Notes

 Author
   Michael Tucker, 11/09/18
****************************************************************************/
bool Check4Wind(void)
{
  static uint32_t results[1];
  static uint8_t WindSpeed;

  // Analog read of the wind turbine
  ADC_MultiRead(results);

  // Scaling to bin the speed value
  WindSpeed = results[0]/700;

  if ((WindSpeed > 0) && (CurrentState != SpinningBySelf) &&
      GetCurrentTreeState() == TreeActive)
  {
    // Post Wind detected event
    ES_Event_t ThisEvent;
    ThisEvent.EventType   = ES_WIND_DETECTED;
    ThisEvent.EventParam  = WindSpeed;
    PostWindService(ThisEvent);
    ES_PostList01(ThisEvent);

    // Plays note based on wind speed
    ES_Event_t ToneEvent;
    ToneEvent.EventType   = ES_PLAY_NOTE;
    if (WindSpeed != LastWindSpeed) {
      switch (WindSpeed)
      {
        case 1:
          ToneEvent.EventParam = C4;
          PostSpeakerService(ToneEvent);
        break;
        case 2:
          ToneEvent.EventParam = G4;
          PostSpeakerService(ToneEvent);
        case 3:
          ToneEvent.EventParam =  C5;
          PostSpeakerService(ToneEvent);
        case 4:
          ToneEvent.EventParam = G5;
          PostSpeakerService(ToneEvent);
        case 5:
          ToneEvent.EventParam = C6;
          PostSpeakerService(ToneEvent);
        break;
      }
    }

    LastWindSpeed = WindSpeed;

    return true;
  }

  return false;
}

/***************************************************************************
 private functions
 ***************************************************************************/

 /****************************************************************************
  Function
     SpinMotor

  Parameters
    None

  Returns
    None

  Description
    Starts PWM for the wind turbine motor and turns the relay on

  Notes

  Author
    Michael Tucker 11/25/18
 ****************************************************************************/
void SpinMotor(void)
{
  PWM_Motor();
  RelayON();
}

/****************************************************************************
 Function
    StopMotor

 Parameters
   None

 Returns
   None

 Description
   Turns relay off and stops PWM of motor

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void StopMotor(void)
{
  RelayOFF();
  Stop_PWM_Motor();
}

/****************************************************************************
 Function
   PWM_Motor

 Parameters
   None

 Returns
   None

 Description
   PWMs motor

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void PWM_Motor(void)
{
  PWM_TIVA_SetDuty(10, 4);
}

/****************************************************************************
 Function
   Stop_PWM_Motor

 Parameters
   None

 Returns
   None

 Description
   Opens up transistor controlling motor speed to enable reading input

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void Stop_PWM_Motor(void)
{
  PWM_TIVA_SetDuty(100, 4);
}

/****************************************************************************
 Function
   RelayON

 Parameters
   None

 Returns
   None

 Description
   Turns on the relay to enable motor control

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void RelayON(void)
{
  HWREG(PIN_BASE+(GPIO_O_DATA+ALL_BITS)) |= RELAY_PIN;
}

/****************************************************************************
 Function
   RelayOFF

 Parameters
   None

 Returns
   None

 Description
   Turns off the relay to enable analog input of motor speed

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void RelayOFF(void)
{
  HWREG(PIN_BASE+(GPIO_O_DATA+ALL_BITS)) &= ~RELAY_PIN;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
