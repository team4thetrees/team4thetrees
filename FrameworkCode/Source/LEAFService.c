/****************************************************************************
 Module
   LEAFService.c

 Revision
   1.0.0

 Description
   This is a file for implementing the LEAF detection service
	 reporting events to the TreeService.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/09/18 16:36 dk      began conversion from TemplateService.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "LEAFService.h"
#include "TreeService.h"
#include "EventCheckers.h"
#include "SpeakerService.h"
#include <PWM16Tiva.h>
#include "BatteryService.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define LEAF_INPUT_PIN BIT0HI
#define HI true
#define LEAF_TIMER 12
#define ONE_SEC 976

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void MarkLEAF(void);
static void ResetMarker(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static LEAFState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitLEAFService

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Michael Tucker 11/10/18
****************************************************************************/
bool InitLEAFService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
	CurrentState = InitLEAF;

  // Initialize leaf detection input
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  while ((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1)
  {
  }
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= LEAF_INPUT_PIN;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= ~LEAF_INPUT_PIN;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostLEAFService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostLEAFService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunLEAFService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Responds to insertion and removal of a LEAF, detected based on input
	 from the trans-resistive circuit.

 Notes

 Author
   Danielle Katz, 11/09/18, 16:48
****************************************************************************/
ES_Event_t RunLEAFService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState){

    // Initializing leaf detection
    case InitLEAF :
      // If Leaf detection is intialized, resets marker position and
      // moves to no leaf detected state
      if ( ThisEvent.EventType == ES_INIT )
      {
        puts("\r\n LEAF DECTECT INITIALIZED \r\n");
        ResetMarker();
        CurrentState = NoLEAFDetected;
      }
      break;

    // No leaf is detected
    case NoLEAFDetected :
      // If the leaf is detected, plays tune and moves to leaf detected state
      if (ThisEvent.EventType == ES_LEAF_DETECTED)
      {
        puts("\r\n LEAF DETECTED BY SENSOR");

        uint16_t tune[5] = {C4, C5, B5, F5, B5};
        PlayTune(tune,5);

        CurrentState = LEAFDetected;
      }
      break;

      // If the LEAF timer times out, resets the marker position
      if (ThisEvent.EventType == ES_TIMEOUT)
      {
        ResetMarker();
      }

    // Leaf is detected
		case LEAFDetected :
      // If leaf is removed, moves back to no leaf detected state
			if (ThisEvent.EventType == ES_LEAF_REMOVED)
			{
				puts("\r\n NO LEAF");
        CurrentState = NoLEAFDetected;
			}

      // If all actions are completed or the 60s timer runs out, marks the leaf
      // and starts the leaf timer
      if (ThisEvent.EventType == ES_ALL_ACTIONS_COMPLETE ||
          ThisEvent.EventType == ES_TREE_TIME_DONE)
      {
        puts("\r\n MARK LEAF");

        MarkLEAF();

        ES_Timer_InitTimer(LEAF_TIMER, ONE_SEC);
      }

      // If the LEAF timer times out, resets the marker position
      if (ThisEvent.EventType == ES_TIMEOUT)
      {
        ResetMarker();
      }
		}

  return ReturnEvent;
}

/****************************************************************************
 Function
   Check4LEAF

 Parameters
   None

 Returns
   bool, true if leaf is detected, false otherwise

 Description
   Checks if a leaf is inserted or removed into the TREE

 Notes

 Author
   Michael Tucker, 11/09/18
****************************************************************************/
bool Check4LEAF(void) {
 static bool LastInputState = false; // Previous state of input
 static bool InputState; // State of transresistive circuit
 ES_Event_t ThisEvent;
 bool ReturnVal = false;

 //Read the Tiva pin that has the input from the trans-resistive circuit
 InputState = ((HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS)) & LEAF_INPUT_PIN)
                == LEAF_INPUT_PIN);

 if (InputState != LastInputState)
 {
   if (InputState == HI)
   {
     ThisEvent.EventType = ES_LEAF_DETECTED;
     PostLEAFService(ThisEvent);
     PostTreeService(ThisEvent);
     PostBatteryService(ThisEvent);
     ReturnVal = true;
   }
   else
   {
     ThisEvent.EventType = ES_LEAF_REMOVED;
     PostLEAFService(ThisEvent);
     PostTreeService(ThisEvent);
     ReturnVal = true;
   }
 }
 LastInputState = InputState;
 return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/

 /****************************************************************************
  Function
    MarkLEAF

  Parameters
    None

  Returns
    None

  Description
    Moves RC servo to mark leaf

  Notes

  Author
    Michael Tucker 11/25/18
 ****************************************************************************/
void MarkLEAF(void)
{
  PWM_TIVA_SetPeriod(25000, 1);
  PWM_TIVA_SetPulseWidth(800, 2);
}

/****************************************************************************
 Function
   MarkLEAF

 Parameters
   None

 Returns
   None

 Description
   Moves RC servo back to home position

 Notes

 Author
   Michael Tucker 11/25/18
****************************************************************************/
void ResetMarker(void)
{
  PWM_TIVA_SetPeriod(25000, 1);
  PWM_TIVA_SetPulseWidth(1875, 2);
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
