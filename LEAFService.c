/****************************************************************************
 Module
   LEAFService.c

 Revision
   1.0.0

 Description
   This is a file for implementing the LEAF detection service
	 reporting events to the TreeService.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/09/18 16:36 dk      began conversion from TemplateService.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "LEAFService.h"
#include "TreeService.h"
#include "EventCheckers.h"
#include "SpeakerService.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define LEAF_INPUT_PIN BIT0HI
#define HI true

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static LEAFState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitLEAFService

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitLEAFService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
	CurrentState = InitLEAF;
  
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  while ((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1)
  {
  }
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= LEAF_INPUT_PIN;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= ~LEAF_INPUT_PIN;
  
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostLEAFService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostLEAFService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunLEAFService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Responds to insertion and removal of a LEAF, detected based on input 
	 from the trans-resistive circuit.

 Notes

 Author
   Danielle Katz, 11/09/18, 16:48
****************************************************************************/
ES_Event_t RunLEAFService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState){
    case InitLEAF :
      if ( ThisEvent.EventType == ES_INIT )
      {
        puts("\r\n LEAF DECTECT INITIALIZED \r\n");
        CurrentState = NoLEAFDetected;
      }
      break;
    case NoLEAFDetected :
      Check4LEAF();
      if (ThisEvent.EventType == ES_LEAF_DETECTED) //do I need a separate event for leaf detection?
      {
        puts("\r\n LEAF DETECTED BY SENSOR");
        ES_Event_t ToneEvent;
        ToneEvent.EventType = ES_PLAY_NOTE;
        ToneEvent.EventParam = C4;
        PostSpeakerService(ToneEvent);
        CurrentState = LEAFDetected;
      }
      break;
		case LEAFDetected :
      Check4LEAF();
			if (ThisEvent.EventType == ES_LEAF_REMOVED)
			{
				puts("\r\n NO LEAF");
        CurrentState = NoLEAFDetected;
			}
		}
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

bool Check4LEAF(void) {
  static bool LastInputState = false; // Previous state of input
	static bool InputState; // State of transresistive circuit
	ES_Event_t ThisEvent;
  bool ReturnVal = false;
  //Read the Tiva pin that has the input from the trans-resistive circuit, store value in TransResSignal 
  InputState = ((HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS)) & LEAF_INPUT_PIN) == LEAF_INPUT_PIN);
  
	if (InputState != LastInputState)
  {
    if (InputState == HI)
    {
      ThisEvent.EventType = ES_LEAF_DETECTED;
      PostLEAFService(ThisEvent);
      PostTreeService(ThisEvent);
      ReturnVal = true;
    }
    else
    {
      ThisEvent.EventType = ES_LEAF_REMOVED;
      PostLEAFService(ThisEvent);
      ReturnVal = true;
    }
  }
  LastInputState = InputState;
	return ReturnVal;
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

