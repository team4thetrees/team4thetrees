/****************************************************************************
 Module
   EVChargeService.c

 Revision
   1.0.0

 Description
   This is a file for implementing the EVCharge interaction service
	 reporting events to the TreeService.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/11/18 14:55 dk      began conversion from LEAFService.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "EVChargeService.h"
#include "TreeService.h"
#include "LEAFService.h"
#include "WindService.h"
#include "SpeakerService.h"
//#include "SolarService.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define RIGHT_PLUG_INPUT BIT3HI
#define WRONG_PLUG_INPUT BIT5HI
#define HI true

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

bool ReadInputPin(uint8_t Pin);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static EVChargeState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitEVChargeService

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitEVChargeService(uint8_t Priority)
{
  puts("\r\n Now inside InitEVChargeService \r\n");
  ES_Event_t ThisEvent;

  MyPriority = Priority;
	CurrentState = InitEVCharge;
  //Init Port B pins 3 and 4 on Tiva as a digital input
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  while ((HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1)
  {
  }
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (RIGHT_PLUG_INPUT | WRONG_PLUG_INPUT);
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= ~RIGHT_PLUG_INPUT;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= ~WRONG_PLUG_INPUT;
  
  puts("\r\n EVCharge Initialized \r\n");
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostEVChargeService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostEVChargeService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunEVChargeService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Responds to insertion and removal of a EVCharge, detected based on input 
	 from the trans-resistive circuit.

 Notes

 Author
   Danielle Katz, 11/09/18, 16:48
****************************************************************************/
ES_Event_t RunEVChargeService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState){
    
    case InitEVCharge :
      puts("\r\n Case is InitEVCharge \r\n");
      if ( ThisEvent.EventType == ES_INIT )
      {
        CurrentState = WaitingForPlug;
      }
      break;
    case WaitingForPlug :
      puts("\r\n Waiting for plug \r\n");
      if (ThisEvent.EventType == ES_PLUG_INSERTED_WRONG)
      {
        //PlayTune?
        puts("\r\n Wrong answer");
        CurrentState = PluggedWrong;
      }
      if (ThisEvent.EventType == ES_PLUG_INSERTED_CORRECT)
      {
        //add line to start motor
        //PlayTune?
        puts("\r\n Correct answer");
        CurrentState = PluggedCorrect;
        ES_Event_t ToneEvent;
        ToneEvent.EventType = ES_PLAY_NOTE;
        ToneEvent.EventParam = C4;
        PostSpeakerService(ToneEvent);
      }
      break;

		case PluggedCorrect :
			if (ThisEvent.EventType == ES_PLUG_REMOVED)
			{
				//add line to stop motor
        puts("\r\n Plug Removed \r\n");
        CurrentState = WaitingForPlug;
			}
      break;
    case PluggedWrong :
			if (ThisEvent.EventType == ES_PLUG_REMOVED)
			{
        puts("\r\n Plug Removed \r\n");
				CurrentState = WaitingForPlug;
			}
      break;
		}
  return ReturnEvent;
}

bool Check4Charge(void)
{
  static bool LastWrongPlugState = false; // last input from Tiva pin that reads the state of the 'incorrect' outlet
  static bool LastRightPlugState = false; // last input from Tiva pin that reads the state of the 'correct' outlet
  static bool WrongPlugState; //input from Tiva pin that reads the state of the 'incorrect' outlet
  static bool RightPlugState; //input from Tiva pin that reads the state of the 'correct' outlet
  
	WrongPlugState = ReadInputPin(WRONG_PLUG_INPUT);
 	RightPlugState = ReadInputPin(RIGHT_PLUG_INPUT);

  bool ReturnVal = false;
  ES_Event_t ThisEvent;
	
  if (WrongPlugState != LastWrongPlugState)
  {
    puts("1");
    ReturnVal = true;
    if (WrongPlugState == HI)
    {
      puts("2");
      ThisEvent.EventType = ES_PLUG_INSERTED_WRONG;
      PostEVChargeService(ThisEvent);
    }
    else
    {
      puts("3");
      ThisEvent.EventType = ES_PLUG_REMOVED;
      PostEVChargeService(ThisEvent);
    }
  }
  else if (RightPlugState != LastRightPlugState)
  {
    ReturnVal = true;
    if (RightPlugState == HI)
    {
      ThisEvent.EventType = ES_PLUG_INSERTED_CORRECT;
      PostEVChargeService(ThisEvent);
    }
    else
    {
      ThisEvent.EventType = ES_PLUG_REMOVED;
      PostEVChargeService(ThisEvent);
    }
  }
  LastRightPlugState = RightPlugState;
  LastWrongPlugState = WrongPlugState;
  
	return ReturnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/
 /****************************************************************************
  Function
     ReadInputPin

  Parameters
    uint8_t Pin: the pin to read the value of

  Returns
    bool, true if HI, false if LO

  Description
    Will read the pin state and return true if HI, false if LO

  Notes

  Author
    Michael Tucker 11/04/18
 ****************************************************************************/
 bool ReadInputPin(uint8_t Pin)
 {
   return (HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS)) & Pin) == Pin;
 }

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

